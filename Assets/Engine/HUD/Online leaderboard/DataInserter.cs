﻿using System.Collections;
using TMPro;
using UnityEngine;

public class DataInserter : MonoBehaviour {
    public DataLoader dataLoader;
    public UserData userdata;
    public TextMeshProUGUI latestScore;
    public TextMeshProUGUI comment;
    public TMP_InputField name;
    public TMP_InputField username;
    public TMP_InputField password;
    public TMP_InputField passwordRepeat;
    public GameObject registerWindow;
    public GameObject mainMenu;

    private int inputScore;
    private int inputKills;
    private float inputPlaytime;
    private int inputLevels;
    private string CreateUserURL = "http://hotboi.veeb.eu/php/insert_user.php";
    private string InsertGameResultURL = "http://hotboi.veeb.eu/php/insert_game_result.php";

    public void CreateUser() {
        if (password.text == passwordRepeat.text) {
            CreateUser(name.text, username.text, password.text);
            RegisterSuccessPopup();
        }
    }

    public void CreateUser(string name, string username, string password) {
        WWWForm form = new WWWForm();
        form.AddField("namePost", name);
        form.AddField("usernamePost", username);
        form.AddField("passwordPost", password);
        WWW www = new WWW(CreateUserURL, form);

        //should check if user with same username already exists
    }

    private void RegisterSuccessPopup() {
        name.text = "";
        username.text = "";
        password.text = "";
        passwordRepeat.text = "";
        registerWindow.SetActive(false);
        mainMenu.SetActive(true);
        //RegisterSuccessPopup.SetActive(true);
    }

    public void UploadResultToDB() {
        StartCoroutine(UploadResult(userdata.GetUsername(),
                userdata.password, (int)userdata.score.Value,
                (int)userdata.GetKills().Value, (int)userdata.GetPlaytime().Value, (int)userdata.level.Value));
    }


    IEnumerator UploadResult(string username, string password, int score, int kills, int playtime, int level) {
        SendScore(score);
        WWWForm form = new WWWForm();
        form.AddField("usernamePost", username);
        form.AddField("passwordPost", password);
        form.AddField("scorePost", score);
        form.AddField("killsPost", kills);
        form.AddField("playtimePost", playtime);
        form.AddField("levelPost", level);
        WWW www = new WWW(InsertGameResultURL, form);
        yield return www;
        print("Upload result: " + www.text);
        if (www.text == "everything ok") {
            dataLoader.LoadPrivateData();
        }
    }

    public void SendScore(int score) {
        latestScore.text = score.ToString();
        comment.text = GetComment(score);
    }

    private string GetComment(int score) {
        if (score > 1000) return "okay, that was pretty nice";
        if (score > 500) return "tell your mother I said hi";
        if (score > 200) return "quite an achievement.. or is it?";
        if (score > 100) return "nice, but can do better..";
        if (score > 10) return "was that it?";
        return "how's that even possible?";
    }
}
